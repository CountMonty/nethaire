from FlaskApp import app, db
from FlaskApp.models import User
from datetime import datetime
import pytz, tzlocal
from pytz import timezone
import xml.etree.ElementTree as ET
import requests, base64

def datetimefilter(value, format="%A, %B %d, %Y at %I:%M%p"):
    tz = pytz.timezone('America/Phoenix') # timezone you want to convert to from UTC
    utc = pytz.timezone('UTC')
    value = utc.localize(value, is_dst=None).astimezone(pytz.utc)
    local_dt = value.astimezone(tz)
    return local_dt.strftime(format)

app.jinja_env.filters['datetimefilter'] = datetimefilter

def plexAuth(username, password):
    url = "https://plex.tv/users/sign_in.json"                                          # Set url for request
    b64string = base64.b64encode(('%s:%s' % (username, password)).encode('utf-8'))      # Encode name:pass for auth
    headers = {'Authorization': "Basic %s" % b64string.decode('utf-8'),
                    'X-Plex-Client-Identifier': "NethairianSea",
                    'X-Plex-Product': "Nethaire",
                    'X-Plex-Version': "1"}                                              # Configure headers needed
    r = requests.post(url, headers=headers)                                             # Authenticate against Plex
    return r

def plexServerAuth(username):
    if username in ["Nethaire", "Nethaire@gmail.com"]:                                  # Admin is not in the list but needs to return true
        return True # If new login def is accepted, delete this precheck

    url = "https://plex.tv/api/users/"
    headers={"X-Plex-Token":app.config['SERVER_TOKEN']}
    r = requests.get(url, headers=headers)
    userTree = ET.fromstring(r.content)

    for user in userTree:
        if username in [user.attrib['username'], user.attrib['email']]:                 # Check if param username is in server list
            return True                                                                 # User is in server list
    return False                                                                        # User is not in server list

def createPlextoLocal(username, email, password):
    '''
    if '@' in username:
        newUser = User(username="", email=username)
    else:
        newUser = User(username=username, email="")
    '''
    newUser = User(username=username, email=email)
    newUser.set_password(password)
    db.session.add(newUser)
    db.session.commit()
    return newUser
