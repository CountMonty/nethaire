
from flask import render_template, url_for, redirect, request, flash, json
from flask_login import login_user, logout_user, current_user, login_required
from FlaskApp import app, login, db
from FlaskApp.forms import LoginForm, PostForm
from FlaskApp.models import User, Post, OAuth
from FlaskApp.utils import plexAuth, plexServerAuth, createPlextoLocal
from flask_dance.consumer.backend.sqla import SQLAlchemyBackend
from flask_dance.consumer import oauth_authorized
from sqlalchemy.orm.exc import NoResultFound
from flask_dance.contrib.google import make_google_blueprint
from collections import OrderedDict
import os

blueprint = make_google_blueprint(
    client_id=app.config['GOOGLE_CLIENT_ID'],
    client_secret=app.config['GOOGLE_CLIENT_SECRET'],
    scope=["profile", "email"]
)
app.register_blueprint(blueprint, url_prefix="/login")

blueprint.backend = SQLAlchemyBackend(OAuth, db.session, user=current_user)

# ROUTES -------------------------------------------------------------------------------------------------------------------------------

network = False			# False for local, true for global

# Public Pages -------------------------------------------------------------------------------------------------------------------------


@app.route('/')
@app.route('/index')
def index():
    loginform = LoginForm()
    postform = PostForm()
    posts = None
    try: posts = Post.query.filter_by(user_id=User.query.filter_by(username='Nethaire').first().id).order_by(Post.timestamp.desc()).all()
    except:
        createPlextoLocal(os.environ.get('ADMIN_NAME'), os.environ.get('ADMIN_EMAIL'), os.environ.get('ADMIN_PW'))
        posts = Post.query.filter_by(user_id=User.query.filter_by(username='Nethaire').first().id).order_by(Post.timestamp.desc()).all()
    return render_template('index.html', loginform=loginform, postform=postform, posts=posts)


@app.route('/resume')
def resume():
    loginform = LoginForm()
    #data = json.load(open(os.path.join(app.root_path, "static", "resume.json")), object_pairs_hook=OrderedDict, strict=False) # For testing
    data = json.load(open(os.path.join('/config', 'resume.json')), object_pairs_hook=OrderedDict, strict=False)
    return render_template('resume.html', loginform=loginform, data=data)


@app.errorhandler(404)
def not_found_error(error):
    loginform = LoginForm()
    return render_template('fourOhfour.html', loginform=loginform), 404

# Authentication Links -----------------------------------------------------------------------------------------------------------------


@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(request.referrer)
    form = LoginForm()
    if form.validate_on_submit():
        un = form.username.data
        pw = form.password.data
        user = User.query.filter_by(username=un).first()					# Find the unique user
        if user is None:
            user = User.query.filter_by(email=un).first()					# If input was via email, find user
        if user is not None and user.check_password(pw):					# Ensure user is found, check the password
            login_user(user, remember=form.remember_me.data)				# If successful, log the user in
            return redirect(request.referrer)								# Return to the page the user logged in on
        else:																# User does not exist in local db
            r = plexAuth(un, pw)                                            # Authenticate against Plex
            if r.status_code == 201:                                        # If PlexAuth succeeds
                data = r.json()["user"]										# Grab the user's Plex data
                userN = data["username"]
                email = data["email"]
                if plexServerAuth(un):                                      # Check if Plex server knows the user
                    user = createPlextoLocal(userN, email, pw)              # Create the user locally if so
                    login_user(user, remember=form.remember_me.data)        # If successful, log the user in
                    return redirect(request.referrer)						# Return to the page the user logged in on
                #flash("plexServerAuth failed: " + un)
            #flash("PlexAuth Failed")
            # flash(r.status_code)
        return redirect(request.referrer)									# Return to page
    return redirect(request.referrer)										# Return to page


@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for("index"))

# Database Utility Links --------------------------------------------------------------------------------------------------------------


@app.route('/post', methods=['GET', 'POST'])
@login_required
def post():
    form = PostForm()
    if form.validate_on_submit():
        post = Post(title=form.title.data, body=form.post.data, author=current_user)
        db.session.add(post)
        db.session.commit()
        return redirect(request.referrer)
    return redirect(request.referrer)


@app.route('/delpost', methods=['GET', 'POST'])
@login_required
def delpost():
    post = Post.query.filter_by(id=request.form['postID']).first()
    db.session.delete(post)
    db.session.commit()
    return redirect(request.referrer)

# create/login local user on successful OAuth login


@oauth_authorized.connect_via(blueprint)
def google_logged_in(blueprint, token):
    if not token:
        flash("Failed to log in with Google.", category="error")
        return False
    resp = blueprint.session.get("/oauth2/v2/userinfo")
    if not resp.ok:
        msg = "Failed to fetch user info from Google."
        #flash(msg, category="error")
        return False
    googleInfo = resp.json()
    googleUserID = str(googleInfo["id"])
    query = OAuth.query.filter_by(provider=blueprint.name, provider_user_id=googleUserID)
    try:
        oauth = query.one()
    except NoResultFound:
        oauth = OAuth(provider=blueprint.name, provider_user_id=googleUserID, token=token)
    if oauth.user:
        login_user(oauth.user)
        #flash("Successfully signed in with Google.")
    else:
        if plexServerAuth(googleInfo["email"]):
            user = User(email=googleInfo["email"], username=googleInfo["name"])
            oauth.user = user
            db.session.add_all([user, oauth])
            db.session.commit()
            login_user(user)
            flash("Successfully signed in with Google.")
        else:
            flash("Could not log in, user is not in server listing")
            pass
    return False

# Semi-Private Servers -----------------------------------------------------------------------------------------------------------------


@app.route('/plex')
@login_required
def plex():
    return render_template('plex.html', network=network)


@app.route('/ombi')
@login_required
def ombi():
    return render_template('ombi.html', network=network)


@app.route('/ftp')
@login_required
def ftp():
    return render_template('ftp.html', network=network)

# Private Servers ----------------------------------------------------------------------------------------------------------------


@app.route('/radarr')
@login_required
def radarr():
    if current_user.username == "Nethaire":
        return render_template('radarr.html', network=network)
    else:
        return render_template('fourOhfour.html')


@app.route('/sonarr')
@login_required
def sonarr():
    if current_user.username == "Nethaire":
        return render_template('sonarr.html', network=network)
    else:
        return render_template('fourOhfour.html')


@app.route('/transmission')
@login_required
def transmission():
    if current_user.username == "Nethaire":
        return render_template('transmission.html', network=network)
    else:
        return render_template('fourOhfour.html')


@app.route('/jackett')
@login_required
def jackett():
    if current_user.username == "Nethaire":
        return render_template('jackett.html', network=network)
    else:
        return render_template('fourOhfour.html')
